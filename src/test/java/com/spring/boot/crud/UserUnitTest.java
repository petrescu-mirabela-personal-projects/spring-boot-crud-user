package com.spring.boot.crud;


import com.spring.boot.crud.entities.User;
import org.junit.Test;


import static org.assertj.core.api.Assertions.assertThat;

public class UserUnitTest {

    @Test
    public void when_CalledGetName_thenCorrect(){
        User user = new User("Julie", "julie@domain.com");
        assertThat(user.getName()).isEqualTo("Julie");
    }

    @Test
    public void whenCalledGetEmail_thenCorrect(){
        User user = new User("Julie", "julie@domain.com");
        assertThat(user.getEmail()).isEqualTo("julie@domain.com");
    }

    @Test
    public void whenCalledSetEmail_thenCorrect(){
        User user = new User("Julie", "julie@domain.com");
        user.setEmail("john@domain.com");
        assertThat(user.getEmail()).isEqualTo("john@domain.com");
    }

    @Test
    public void whenCalledToString_thenCorrect(){
        User user = new User("Julie", "julie@domain.com");
        assertThat(user.toString()).isEqualTo("User{id=0, name=Julie, email=julie@domain.com}");
    }
}
