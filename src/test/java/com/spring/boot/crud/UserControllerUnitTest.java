package com.spring.boot.crud;

import com.spring.boot.crud.controllers.UserController;
import com.spring.boot.crud.entities.User;
import com.spring.boot.crud.repositories.UserRepository;
import org.junit.Test;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;

import org.junit.BeforeClass;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class UserControllerUnitTest {

    private static UserController userController;
    private static UserRepository mockedUserRepository;
    private static BindingResult mockedBindingResult;
    private static Model mockedModel;

    /*
    When we want to execute an expensive common operation before each test,
    it's preferable to execute it only once before running all tests using @BeforeClass.
     */
    @BeforeClass
    public static void setUpUserControllerInstance(){
        /*
        @Mock to create and inject mocked instances without having to call Mockito.mock manually.
         */
        mockedUserRepository = mock(UserRepository.class);
        mockedBindingResult = mock(BindingResult.class);
        mockedModel = mock(Model.class);
        userController = new UserController(mockedUserRepository);
    }

    @Test
    public void whenCalledshowSignUpForm_thenCorrect(){
        User user = new User("John", "john@domain.com");
        assertThat(userController.showSignUp(user)).isEqualTo("add-user");
    }

    @Test
    public void whenCalledandUserAndValidUser_thenCorrect(){
        User user = new User("John", "john@domain.com");
        when(mockedBindingResult.hasErrors()).thenReturn(false);
        // in case of NO error list all the users
        assertThat(userController.addUser(user, mockedBindingResult, mockedModel)).isEqualTo("index");
    }

    @Test
    public void whenCalledandUserAndInValidUser_thenCorrect() {
        User user = new User("John", "john@domain.com");
        when(mockedBindingResult.hasErrors()).thenReturn(true);
        // in case of an error then go to add user again
        assertThat(userController.addUser(user, mockedBindingResult, mockedModel)).isEqualTo("add-user");
    }

    @Test(expected = IllegalArgumentException.class)
    public void whenCalledshowUpdateForm_thenIllegalArgumentException() {
        try {
            // try to update a NOK user id, here 0
            assertThat(userController.showUpdateForm(0, mockedModel)).isEqualTo("update-user");
        }catch(IllegalAccessError e) {
            assertThat(new Integer(1)).isEqualTo(new Integer(1));
        }
    }

    @Test
    public void whenCalledupdateUserAndValidUser_thenCorrect(){
        User user = new User("John", "john@domain.com");
        when(mockedBindingResult.hasErrors()).thenReturn(false);
        // if there is NO error then list the users, considering the updated user
        assertThat(userController.updateUser(1l, user, mockedBindingResult, mockedModel)).isEqualTo("index");
    }

    @Test
    public void whenCalledupdateUserAndInValidUser_thenCorrect() {
        User user = new User("John", "john@domain.com");

        when(mockedBindingResult.hasErrors()).thenReturn(true);

        assertThat(userController.updateUser(1l, user, mockedBindingResult, mockedModel)).isEqualTo("update-user");
    }

    @Test(expected = IllegalArgumentException.class)
    public void whenCalleddeleteUser_thenIllegalArgumentException() {
        assertThat(userController.deleteUser(1l, mockedModel)).isEqualTo("index");
    }
}
