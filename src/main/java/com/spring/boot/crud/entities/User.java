package com.spring.boot.crud.entities;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

/*
 The JPA implementation, which is Hibernate, in this case, will be able to perform CRUD operations on the domain entities.
 */
@Entity
@Table(name = "user", schema = "personal_projects")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    /*
    @NotBlank - This implies that we can use Hibernate Validator for validating the constrained fields
    before persisting or updating an entity in the database.
     */
    @NotBlank(message = "Name is mandatory")
    private String name;

    @NotBlank(message = "Email is mandotory")
    private String email;

    public User() {
    }

    public User(@NotBlank(message = "Name is mandatory") String name, @NotBlank(message = "Email is mandotory") String email) {
        this.name = name;
        this.email = email;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                '}';
    }
}
