package com.spring.boot.crud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

//LINK: https://www.baeldung.com/spring-boot-crud-thymeleaf

@SpringBootApplication
@EnableAutoConfiguration
@ComponentScan(basePackages={"com.spring.boot.crud"})
@EnableJpaRepositories(basePackages="com.spring.boot.crud.repositories")
@EnableTransactionManagement
@EntityScan(basePackages="com.spring.boot.crud.entities")
public class SpringBootCrudUserApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootCrudUserApplication.class, args);
	}

}
