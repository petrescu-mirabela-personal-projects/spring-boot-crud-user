## Spring boot CRUD

This module contains articles about Spring Boot CRUD Operations

### Relevant Articles:
- [Spring Boot CRUD Application with Thymeleaf](https://www.baeldung.com/spring-boot-crud-thymeleaf)
- [Using a Spring Boot Application as a Dependency](https://www.baeldung.com/spring-boot-dependency)
- [Using github project] (https://gitlab.com/petrescu-mirabela-personal-projects/spring-boot-crud-user)

# Viewing the running application
To view the running application, visit [http://localhost:8085/signup](http://localhost:8080/signup) in your browser